import java.nio.FloatBuffer;

public class ObjModifier {

    // FloatBuffer stores the vertices of a mesh in a continuous array of floats (see below)
    // [x0, y0, z0, x1, y1, z1, ..., xn, yn, zn]
    // This kind of layout is common in low-level 3D graphics APIs.
    public static void centerVertices(FloatBuffer buf) {
        float minX = Float.MAX_VALUE;
        float maxX = Float.MIN_VALUE;
        float minY = Float.MAX_VALUE;
        float maxY = Float.MIN_VALUE;
        float minZ = Float.MAX_VALUE;
        float maxZ = Float.MIN_VALUE;

        for (int i = 0; i + 2 < buf.capacity(); i++) {
            minX = getLesser(buf.get(i), minX);
            maxX = getGreater(buf.get(i), maxX);
            i++;
            minY = getLesser(buf.get(i), minY);
            maxY = getGreater(buf.get(i), maxY);
            i++;
            minZ = getLesser(buf.get(i), minZ);
            maxZ = getGreater(buf.get(i), maxZ);
        }
        float shiftX = (minX + maxX) / 2;
        float shiftY = (minY + maxY) / 2;
        float shiftZ = (minZ + maxZ) / 2;

        for (int i = 0; i + 2 < buf.capacity(); i++) {
            buf.put(i, buf.get(i) - shiftX);
            buf.put(++i, buf.get(i) - shiftY);
            buf.put(++i, buf.get(i) - shiftZ);
        }
    }

    private static float getLesser(float v1, float v2) {
        return v1 < v2 ? v1 : v2;
    }

    private static float getGreater(float v1, float v2) {
        return v1 > v2 ? v1 : v2;
    }
}
